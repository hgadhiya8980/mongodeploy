
import json
from flask import (flash, Flask, redirect, render_template, request)
from pymongo import MongoClient
from flask_cors import CORS

app = Flask(__name__)
CORS(app)


app.config["SECRET_KEY"] = "sdfsf65416534sdfsdf4653"
secure_type = "http"

client = MongoClient("mongodb+srv://jaykakadiyawork:FB30fRUM8DV0U1xk@cluster0.wwspsfj.mongodb.net/?retryWrites=true&w=majority")

def register_data(db_name, coll_name, new_dict):
    try:
        db = client[db_name]
        coll = db[coll_name]
        coll.insert_one(new_dict)

        return "add_data"

    except Exception as e:
        print(e)

def find_all_cust_details_coll(db_name, coll_name):
    try:
        db = client[db_name]
        coll = db[coll_name]
        res = coll.find({})
        return res

    except Exception as e:
        print(e)

def find_all_specific_user(db_name,coll_name, di):
    try:
        db = client[db_name]
        coll = db[coll_name]
        res = coll.find(di)
        return res

    except Exception as e:
        print(e)

@app.route("/", methods=["GET", "POST"])
def home():
    try:
        return {"Add data Route": "/add_data","get data Route": "/get_all_data","get specific data Route": "/get_spec_data"}
    
    except Exception as e:
        return {"status":500, "message": "server error"}


@app.route("/add_data", methods=["GET", "POST"])
def add_data():
    try:
        if request.method == "POST":
            data = json.loads(request.data)
            coll_name = data.get("coll_name", "nothing")
            new_dict = data.get("new_dict", {"dtaa","data"})
            db_name = data.get("db_name", "none")
            register_data(coll_name=coll_name, new_dict=new_dict, db_name=db_name)
            return {"status":200, "message": "data added"}
        else:
            return {"status":500, "message": "server error"}
    
    except Exception as e:
        return {"status":500, "message": "server error"}
    
@app.route("/get_all_data", methods=["GET", "POST"])
def get_all_data():
    try:
        if request.method == "POST":
            data = json.loads(request.data)
            coll_name = data.get("coll_name", "customer_details")
            db_name = data.get("db_name", "none")
            all_data = find_all_cust_details_coll(coll_name=coll_name, db_name=db_name)
            keys = []
            values = []
            for var in all_data:
                get_keys = list(var.keys())
                get_values = list(var.values())
                get_keys.pop(0)
                get_values.pop(0)
                if get_keys not in keys:
                    keys.append(get_keys)
                if get_values not in values:
                    values.append(get_values)
                       
            return {"keys":keys, "values": values}
        else:
            return {"status":500, "message": "server error"}
    
    except Exception as e:
        return {"status":500, "message": "server error"}
    
@app.route("/get_spec_data", methods=["GET", "POST"])
def get_spec_data():
    try:
        if request.method == "POST":
            data = json.loads(request.data)
            coll_name = data.get("coll_name", "customer_details")
            db_name = data.get("db_name", "none")
            new_dict = data.get("new_dict", {"dtaa","data"})
            all_data = find_all_specific_user(coll_name=coll_name, di=new_dict, db_name=db_name)
            keys = []
            values = []
            for var in all_data:
                get_keys = list(var.keys())
                get_values = list(var.values())
                get_keys.pop(0)
                get_values.pop(0)
                if get_keys not in keys:
                    keys.append(get_keys)
                if get_values not in values:
                    values.append(get_values)
                       
            return {"keys":keys, "values": values}
        else:
            return {"status":500, "message": "server error"}
    
    except Exception as e:
        return {"status":500, "message": "server error"}

if __name__ == "__main__":
    app.run()
